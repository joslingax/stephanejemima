<?php

use App\Http\Controllers\InviteController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\TableController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('admin', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/dashboard', function () {
    return view('welcome');
})->name('admin.dashboard');

Route::resource('invites',InviteController::class)->middleware(['auth']);
Route::resource('playlists',PlaylistController::class)->middleware(['auth']);
Route::resource('tables',TableController::class)->middleware(['auth']);
