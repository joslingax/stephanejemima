<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Ces données n'ont pas de référence dans notre BDD.",
    'password' => 'Le mot de passe fourni est incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
