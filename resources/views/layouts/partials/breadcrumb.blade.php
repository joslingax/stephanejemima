<ol class="breadcrumb df-breadcrumbs mg-b-40">
    <li class="breadcrumb-item">
        <a href="{{ route('dashboard') }}">Accueil</a>
    </li>
    @isset($links)
        @foreach ($links as $key => $link)
            @php
                $class = 'breadcrumb-item';

                if ($loop->last) $class .= ' active';

            @endphp
            <li class="{{ $class }}">
                @if ($loop->last)
                    <span data-url="{{ $link['url'] }}">{{ $link['title'] }}</span>
                @else
                    <a href="{{ $link['url'] }}">
                        {{ $link['title'] }}
                    </a>
                @endif

            </li>
        @endforeach
    @endisset
</ol>
