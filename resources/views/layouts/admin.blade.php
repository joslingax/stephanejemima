<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Page d'authentification">
    <meta name="author" content="{{ env('AUTHOR') }}">
    <meta name="geo.region" content="GA-1" />
    <meta name="geo.placename" content="Libreville" />
    <meta name="geo.position" content="0.413987;9.459993" />
    <meta name="ICBM" content="0.413987, 9.459993" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <meta http-equiv="Content-Security-Policy" content="base-uri 'self'; object-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval';"> --}}
    <title>{{ env('APP_NAME') }}</title>
    <link rel="canonical" href="{{ url('/') }}"/>
    <link href="https://fonts.googleapis.com/css2?family=Concert+One&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @stack('styles')
</head>
<body>
  <div id="app">
    @include('layouts.partials.aside')
    <div class="content ht-100v pd-0">
        @include('layouts.partials.header')
        <div class="content-body">
            @yield('content')
            <!-- <flash type="{{ session('type') }}" message="{{ session('status') }}"></flash> -->
        </div>
    </div>
  </div>
  <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
