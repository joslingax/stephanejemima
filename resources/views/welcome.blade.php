<!-- *
* LEGEND - Iconic Coming Soon Template
* Build Date: January 2016
* Last Update: 16th November 2017
* Author: Madeon08
* Copyright (C) 2018 Madeon08
* This is a premium product available exclusively here : https://themeforest.net/user/Madeon08/portfolio
* -->
<!DOCTYPE html>
<html lang="en-us" class="no-js">

	<head>
		<meta charset="utf-8">
        <title>HS / JR</title>
        <meta name="description" content="The description should optimally be between 150-160 characters.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Madeon08">

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Retina iPad Touch Icon-->
        <link rel="apple-touch-icon" sizes="144x144" href="img/favicon-retina-ipad.png">
        <!-- Retina iPhone Touch Icon-->
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon-retina-iphone.png">
        <!-- Standard iPad Touch Icon--> 
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon-standard-ipad.png">
        <!-- Standard iPhone Touch Icon--> 
        <link rel="apple-touch-icon" sizes="57x57" href="img/favicon-standard-iphone.png">

        <!-- ============== Resources style ============== -->
        <link rel="stylesheet" href="css/style.css" />

		<!-- Modernizr runs quickly on page load to detect features -->
		<script src="js/modernizr.custom.js"></script>
	</head>
	
	<body>

		<!-- Page preloader -->
		<div id="loading">
			<div id="preloader">
				<span></span>
				<span></span>
			</div>
		</div>

		<!-- Overlay -->
		<div class="global-overlay">

			<canvas id="constellationel"></canvas>

			<div class="overlay vegas-container" style="height: 657px;">
				<div class="vegas-slide vegas-transition-fade vegas-transition-fade-in" style="transition: all 1000ms ease 0s;">
					<div class="vegas-slide-inner vegas-animation-kenburns" style="background-image: url(&quot;https://static.wixstatic.com/media/1948b8_46c4033a611a409d8c787fd1757234f0~mv2.jpeg/v1/fill/w_1349,h_842,al_c,q_85,usm_0.66_1.00_0.01/1948b8_46c4033a611a409d8c787fd1757234f0~mv2.webp&quot;);background-color: rgba(0, 0, 0, 0);background-position: center center;background-size: cover;animation-duration: 5000ms;"></div></div><div class="vegas-wrapper" style="overflow: hidden; padding: 0px;"><div class="overlay-dash"></div></div></div>
		</div>

		<!-- START - Home Part -->
		<section id="home-wrap">

			<!-- Stars Overlay - Uncomment the next 3 lines to activate the effect-->
			<!-- <div id='stars'></div>
			<div id='stars2'></div>
			<div id='stars3'></div> -->

			<div class="content">

				<!-- Your logo -->
				<svg preserveAspectRatio="xMidYMid meet" data-bbox="22.535 27.206 154.93 145.588" viewBox="22.535 27.206 154.93 145.588" height="200" width="200" xmlns="http://www.w3.org/2000/svg" data-type="color" role="img">
					<g>
						<path d="M144.842 107.548c-20.451 0-37.372 16.16-44.842 24.639-7.471-8.479-24.392-24.639-44.842-24.639-17.989 0-32.623 14.635-32.623 32.623s14.635 32.623 32.623 32.623c20.451 0 37.371-16.16 44.842-24.638 7.471 8.479 24.392 24.638 44.842 24.638 17.989 0 32.623-14.635 32.623-32.623s-14.634-32.623-32.623-32.623zM83.63 150.109C71.406 160.508 61.4 162.69 55.158 162.69c-12.417 0-22.519-10.102-22.519-22.519s10.102-22.519 22.519-22.519c17.372 0 32.659 15.808 38.371 22.526a87.964 87.964 0 0 1-9.899 9.931zm61.212 12.581c-17.366 0-32.649-15.797-38.365-22.519 5.716-6.722 20.999-22.519 38.365-22.519 12.417 0 22.519 10.102 22.519 22.519 0 12.417-10.101 22.519-22.519 22.519z" fill="#FFF" data-color="1"></path>
						<path d="M89.016 97.168c3.395 2.626 8.038 5.644 8.234 5.771l2.751 1.785 2.75-1.785c.196-.127 4.839-3.145 8.234-5.771 7.782-6.019 16.714-14.166 16.714-23.818 0-9.931-8.08-18.012-18.012-18.012A18.014 18.014 0 0 0 100 58.169a18.014 18.014 0 0 0-9.687-2.831c-9.932 0-18.012 8.08-18.012 18.012.001 9.652 8.933 17.799 16.715 23.818zm1.297-31.726c2.254 0 4.409.969 5.911 2.659L100 72.347l3.775-4.246a7.916 7.916 0 0 1 5.911-2.659c4.36 0 7.908 3.547 7.908 7.908 0 3.936-4.304 9.26-12.792 15.825-1.495 1.156-3.32 2.437-4.803 3.449-1.482-1.012-3.307-2.293-4.802-3.449-8.487-6.565-12.791-11.89-12.791-15.825 0-4.36 3.547-7.908 7.907-7.908z" fill="#FFF" data-color="1"></path>
						<path fill="#FFF" d="M73.952 28.191l8.94 18.322-1.087.53-8.94-18.321 1.087-.531z" data-color="1"></path>
						<path fill="#FFF" d="M126.31 28.193l1.087.53-8.944 18.321-1.087-.53 8.944-18.321z" data-color="1"></path>
						<path fill="#FFF" d="M100.733 27.206v19.568h-1.21V27.206h1.21z" data-color="1"></path>
					</g>
				</svg>

				<h1 class="text-intro opacity-0"><span class="polyfy-title">Jémima Réi  &  Henri-Stéphane </span></h1>

				<p class="text-intro opacity-0">
					Le site web de notre mariage est en construction.
				</p>

                <a href="#" id="go-contact" data-target="contact-anchor" style="display: none;" class="light-btn text-intro opacity-0">Have a question?</a>

              <a data-dialog="somedialog" class="action-btn trigger text-intro opacity-0" style="display: none;">Notify Me!</a>
        
			</div> <!-- /. content -->

			<!-- Social icons -->
			<div class="social-icons text-intro opacity-0">

				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>

			</div> <!-- /. social-icons -->

		</section>
		<!-- END - Home Part -->

		<!-- START - More Informations Part -->

		<!-- END - More Informations Part -->

		<!-- Button Cross to close the More Informations/Right Part -->
		<div class="command-info-wrap">
			
			<!-- Cross to close -->
			<button class="to-close">
				<i class="icon ion-close-round"></i>
			</button>

			<!-- Arrow going down -->
			<div class="to-scroll" data-toggle="tooltip" data-placement="left" title="Scroll to see more...">
				<i class="icon ion-arrow-down-c scroll-down"></i>
			</div>

		</div> <!-- /. command-info-wrap -->

		<!-- START - Newsletter Popup -->

		<!-- END - Newsletter Popup -->

		<!-- Root element of PhotoSwipe, the gallery. Must have class pswp. -->
		<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

		    <!-- Background of PhotoSwipe. 
	        	It's a separate element as animating opacity is faster than rgba(). -->
		    <div class="pswp__bg"></div>

		    <!-- Slides wrapper with overflow:hidden. -->
		    <div class="pswp__scroll-wrap">

		        <!-- Container that holds slides. 
		            PhotoSwipe keeps only 3 of them in the DOM to save memory.
		            Don't modify these 3 pswp__item elements, data is added later on. -->
		        <div class="pswp__container">
		            <div class="pswp__item"></div>
		            <div class="pswp__item"></div>
		            <div class="pswp__item"></div>
		        </div>

		        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
		        <div class="pswp__ui pswp__ui--hidden">

		            <div class="pswp__top-bar">

		                <!--  Controls are self-explanatory. Order can be changed. -->

		                <div class="pswp__counter"></div>

		                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

		                <button class="pswp__button pswp__button--share" title="Share"></button>

		                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

		                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

		                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
		                <!-- element will get class pswp__preloader--active when preloader is running -->
		                <div class="pswp__preloader">
		                    <div class="pswp__preloader__icn">
		                      <div class="pswp__preloader__cut">
		                        <div class="pswp__preloader__donut"></div>
		                      </div>
		                    </div>
		                </div>

		            </div>

		            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
		                <div class="pswp__share-tooltip"></div> 
		            </div>

		            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
		            </button>

		            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
		            </button>

		            <div class="pswp__caption">
		                <div class="pswp__caption__center"></div>
		            </div>

		        </div>

		    </div>

		</div>
		<!-- /. Root element of PhotoSwipe. Must have class pswp. -->

	<!-- ///////////////////\\\\\\\\\\\\\\\\\\\ -->
    <!-- ********** Resources jQuery ********** -->
    <!-- \\\\\\\\\\\\\\\\\\\/////////////////// -->
	
	<!-- * Libraries jQuery, Easing and Bootstrap - Be careful to not remove them * -->
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.easings.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<!-- Accelerated JavaScript animation JS file -->
	<script src="js/velocity.min.js"></script> 

	<!-- Accelerated JavaScript animation UI JS file -->
	<script src="js/velocity.ui.min.js"></script> 

	<!-- Newsletter plugin -->
	<script src="js/notifyMe.js"></script>

	<!-- Contact form plugin -->
	<script src="js/contact-me.js"></script>

	<!-- Slideshow/Image plugin -->
	<script src="js/vegas.js"></script>

	<!-- Scroll plugin -->
	<script src="js/jquery.mousewheel.js"></script>

	<!-- Custom Scrollbar plugin -->
	<script src="js/jquery.mCustomScrollbar.js"></script>

	<!-- Popup Newsletter Form -->
	<script src="js/classie.js"></script>
	<script src="js/dialogFx.js"></script>

	<!-- PhotoSwipe Core JS file -->
	<script src="js/photoswipe.js"></script> 

	<!-- PhotoSwipe UI JS file -->
	<script src="js/photoswipe-ui-default.js"></script>

	<!-- Constellation effect -->
	<script src="js/constellation.js"></script> 

	<!-- Main JS File -->
	<script src="js/main.js"></script>
	
	<!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->

	</body>

</html>