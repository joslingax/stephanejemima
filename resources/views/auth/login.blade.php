@extends('layouts.app')

@section('content')
<div class="media align-items-stretch justify-content-center ht-100p pos-relative">
          <div class="media-body align-items-center d-none d-lg-flex">
            <div class="mx-wd-600">
              <img src="{{ asset('img/img15.png') }}" class="img-fluid" alt="">
            </div>
          </div><!-- media-body -->
          <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60 mt-5">
            <div class="wd-100p">
              <h3 class="tx-color-01 mg-b-5">Connexion</h3>
              <p class="tx-color-03 tx-16 mg-b-40">Svp connectez-vous pour continuer.</p>
            <form method="POST" action="{{ route('login') }}">
              @csrf
              <div class="form-group">
                <label>Adresse e-mail</label>
                <input  name="email" class="form-control @error('email') is-invalid @enderror" placeholder="joslingax@gmail.com">
                @error('email')
                 <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                 </span>
                @enderror
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Mot de passe</label>
                  <a href="" class="tx-13">Mot de passe oublié?</a>
                </div>
                <input  name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter your password">
                @error('password')
                 <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                 </span>
                @enderror
              </div>
              <button type="submit" class="btn btn-brand-02 btn-block">Connexion</button>
            </form>
            </div>
            </div>
          </div><!-- sign-wrapper -->
        </div>
@endsection
