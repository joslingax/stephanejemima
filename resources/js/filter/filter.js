window.Vue = require('vue');

// Filtre Texte
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.toUpperCase()
})

Vue.filter('ucWords', function (value) {
  if (!value) return ''
  var str = value.toLowerCase();
  return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
  	function(s){
  	  return s.toUpperCase();
	});
})


// Filtres temporels
Vue.filter("toDateTime",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  toDateFormat(value,"DD-MM-YYYY à hh:mm")
}); 

Vue.filter("toDate",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  toDateFormat(value,"DD-MM-YYYY")
}); 

Vue.filter("toYear",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  moment(value,"DD-MM-YYYY").year()
}); 
Vue.filter("ago",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  moment(value).fromNow()
}); 