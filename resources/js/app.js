/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');
 window.Vue = require('vue');
 
 import Maska from 'maska'
 Vue.use(Maska)
 
 import { ModalPlugin } from 'bootstrap-vue'
 Vue.use(ModalPlugin)
 
 Vue.prototype.feather = require('feather-icons')
 
 // Méthodes et messages de validation customisés

 
 //Ajout des pages et composants globaux
 //Ajout des filtres globaux
 // Script js dashforge
 import dashforge from "./themes/dashforge.js";
 import Vuetify, { VSkeletonLoader } from 'vuetify/lib'
 import 'vuetify/dist/vuetify.min.css'
 
 
 Vue.use(Vuetify, {
   components: { VSkeletonLoader },
 })
 
 /**
  * Next, we will create a fresh Vue application instance and attach it to
  * the page. Then, you may begin adding components to this application
  * or customize the JavaScript scaffolding to fit your unique needs.
  */
 const app = new Vue({
     el: '#app',
     mounted(){
         dashforge.init()
     },
     methods:{
             /**
              * Indique qu'une tâche est en cours de developpement
              * 
              * @param   {String} functionality Objet passé par l'event.
              * @returns void
              */
             taskUnderDev(functionality)
             {
                 var config = {icon:"fas fa-exclamation-triangle", type : 'orange'}
                 onAlert("La fonctionalité : "+functionality+" est en cours de développement",'Information',config)
             },
     }
 });
 