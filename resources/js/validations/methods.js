import moment from "moment";


/**
 * Contient des règles de validation customisées
 */
const included = {
  getMessage(field, args) {
    // will be added to default locale messages.
    return "ok"
  },
  validate(value, args) {
    return true;
  }
};

// Verifie qu'une chaine ne commence pas par un caractère ou groupe de caractères
// précis
const dontStartsWith = {
  validate(value, { start}) {
    value = ""+value
    return !value.startsWith(start);
  },
  params: ['start'],
  message: (fieldName, placeholders) => {
    return `Le champs ${fieldName} ne doit pas commencer par ${placeholders.start}`;
  }
}

// Verifie qu'une chaine ne commence pas par un caractère ou groupe de caractères
// précis
const dateIsAfter = {
  validate(value, {fin , message}) {

    var actual = moment(value,'L')
    var limit = (fin != "" && fin !=undefined && fin != null) ? moment(fin,'DD/MM/YYYY') : null
    console.log(actual.isAfter(limit));
    
    return  actual ?  actual.isAfter(limit) : true;
  },
  params: ['fin','message'],
  message: (fieldName, placeholders) => {
    return placeholders.message ? placeholders.message : "La date doit être ultérieure à "+placeholders.fin;
  }
}

// Verifie qu'une chaine ne commence pas par un caractère ou groupe de caractères
// précis
const dateIsBefore = {
  validate(value, {fin , message}) {

    var actual = moment(value,'L')
    var limit = (fin != "" && fin !=undefined && fin != null) ? moment(fin,'DD/MM/YYYY') : moment().format('DD/MM/YYYY')
    
    return  actual ?  actual.isBefore(limit) : true;
  },
  params: ['fin','message'],
  message: (fieldName, placeholders) => {
    var fin = placeholders.fin ? placeholders.fin : moment().format('DD/MM/YYYY')
    return placeholders.message ? placeholders.message : "La date doit être antérieure au "+fin;
  }
}

// Verifie qu'une chaine commence par un caractère ou groupe de caractères
// précis
const startsWith = {
  validate(value, { start}) {
    value = ""+value
    return value.startsWith(start);
  },
  params: ['start'],
  message: (fieldName, placeholders) => {
    return `Le champs ${fieldName} ne doit pas commencer par ${placeholders.start}`;
  }
}

const sizeFile = {
  validate(value, { size}) {
    value = ""+value
    return value.startsWith(start);
  },
  params: ['size'],
  message: (fieldName, placeholders) => {
    return "";
  }
}

// Verifie qu'une chaine de caractère n'est pas identique à une autre
// précis
const notEqual = {
  validate(value, {strToCompare}) {
    value = ""+value
    return value.startsWith(start);
  },
  params: ['start'],
  message: (fieldName, placeholders) => {
    return `Le champs ${fieldName} ne doit pas commencer par ${placeholders.start}`;
  }
}

// Verifie qu'une chaine de caractère n'est pas identique à une autre
// précis
const before = {
  validate(value, {date}) {
    if(date){
     return moment(value,'DD/MM/YYYY').isBefore(moment(date,'DD/MM/YYYY'))
    }
    else
    {
      return moment(value,'DD/MM/YYYY').isBefore(moment())
    }
  },
  params: ['date'],
}
// fixe une date minimale pour une date donnée
const date_min = {
  validate(value, {date}) {
    if(date){
     return moment(value,'DD/MM/YYYY').isAfter(moment(date,'DD/MM/YYYY'))
    }
    else
    {
      return false
    }
  },
  params: ['date'],
}

// Verifie qu'une chaine de caractère n'est pas identique à une autre

const date = {
  params: ['format','message'],
  validate(value, {format,field}) {
    return moment(value,format,true).isValid()
  },
  message: (fieldName, placeholders) => {
    return placeholders.message ? placeholders.message : `Le format de la date est invalide`;
  }
};
const personneIsUnique = {
  params: ['personnes'],
  validate(value, {personnes}) {

    var found = personnes.find(p => {
      return  p.nom.toLowerCase() == value.nom.toLowerCase() &&
              p.prenom.toLowerCase() == value.prenom.toLowerCase() && 
              p.sexe.toLowerCase() == value.sexe.toLowerCase() &&
              p.lieu_naissance.toLowerCase() == value.lieu_naissance.toLowerCase() && 
              (!value.hasOwnProperty('uuid') || p.uuid != value.uuid) &&
              p.date_naissance == value.date_naissance 
     })
     
    return found ? false : true
  },
  message: "Cette personne est déjà présente sur la liste."
};
const validator = {
  getMessage(field, args) {
    // will be added to default locale messages.
    // Returns a message.
  },
  validate(value, args) {
    // Returns a Boolean or a Promise that resolves to a boolean.
  }
};
export default { included, dontStartsWith, startsWith, date, dateIsAfter, validator,personneIsUnique, before, date_min} ;