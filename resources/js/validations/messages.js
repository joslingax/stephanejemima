import moment from "moment";


/**
 * Contient la liste des messages de validation customisé de l'application
 * 
 * @author Joslin Issiga <joslin.issiga@aninf.ga>
 */
let validations = {
    email: {
      required : "L'adresse e-mail est requise.",
      email:"Le format  de l'adresse e-mail est invalide",
      max:"L'adresse e-mail doit avoir au plus {length} caractères."
    },
    nom_complet: {
      required : "Le nom de l'établissement est requis.",
      min:"Le nom de l'établissement doit avoir au moins {length} caractères.",
      max:"Le nom de l'établissement doit avoir au plus {length} caractères."
    },
    abbreviation: {
      required : "L'abbréviation est requise.",
      max:"L'abbrévation doit avoir au plus {length} caractères."
    },
    nr_rue: {
      required : "Le n° de rue est requis.",
      max:"Le n° de rue doit avoir au plus {length} caractères."
    },
    nom_rue: {
      required : "Le nom de rue est requis.",
      max:"Le nom de rue doit avoir au plus {length} caractères."
    },
    quartier_id: {
      required : "Le quartier est requis.",
      exists:"Ce quartier est inconnu."
    },
    ville_id: {
      required : "La ville est requise.",
      exists:"Cette ville est inconnue."
    },
    fonction: {
      max:"La fonction doit avoir au plus {length} caractères."
    },
    siteweb :{
      required:"Le nom de domaine est requis.",
      min:"Le domaine  doit avoir au moins {length}  caractères.",
      max:"Le domaine  doit avoir au plus {length}  caractères."
    },
    date_dbt: {
      required : "La date de début est requise.",
      before:'La date de naissance ne doit pas être antérieure au '+moment().format('d/m/y'),
      date:"Ce format de date est invalide."
    },
    date_fin: {
      required : "La date de fin est requise.",
      date:"Ce format de date est invalide."
    },

    piece_identite: {
      required : "La copie de la pièce d'identité est requise.",
    },
    motif_id: {
      required : "Le motif de la demande est requis.",
      exists:"Ce motif est inconnu de nos systèmes."
    },
    motif_document: {
      required : "Le document est requis.",
      size:"La taille du document ne doit pas dépasser 2 Mo.",
      ext:"Le fichier doit être un fichier de type [ pdf, png, jpg]"
    },
    document: {
      required : "Le document est requis.",
      size:"La taille du document ne doit pas dépasser 2 Mo.",
      ext:"Le type de ce fichier n'est pas accepté.",
      mimes:"Ce type de ce fichier n'est pas accepté."
    },
    tranche_horaire_id: {
      required : "La tranche horaire est requise.",
      exists:"Cette tranche horaire est inconnue de nos systèmes."
    },
    password: {
      required : "Le mot de passe est requis.",
      min: " Le mot de passe doit avoir au moins {length} lettres"
      
    },
    password_confirmation: {
      required:"La confirmation du mot de passe est requise.",
      confirmed : "La confirmation du mot de passe ne corresponds pas.",
    },
    nom: {
      required:"Le nom  est requis.",
      min: "Le nom doit avoir au moins {length} lettres",
      max: "Le nom doit avoir au plus {length} lettres"
    },
    motif: {
      required: "Le motif de la demande est requis."
    },
    nom_rue: {
      required:"Le nom de la rue est requis.",
      min: "Le nom de la rue doit avoir au moins {length} caractères",
      max: "Le nom de la rue doit avoir au plus {length} caractères"
    },
    nr_rue: {
      required:"Le n° de la rue est requis.",
      regex:'Le n° doit contenir au plus 3 chiffres.',
      dontStartsWith:'Le n° de rue ne doit pas commencer par {start}.'
    },
    est_boursier: {
      oneOf:"Seules les valeurs 'vrai' et 'faux' sont acceptées",
      required:"Précisez le statut boursier de l'apprenant."
    },
    est_salarie: {
      oneOf:"Seules les valeurs 'vrai' et 'faux' sont acceptées",
      required:"Précisez le statut salarié de l'apprenant."
    },
    province_id: {
      required:"La province est requise.",
      exists:"Cette province est inconnue.",
    },
    pays_id: {
      required:"Le pays est requis.",
      exists:"Ce pays est inconnu.",
    },
    nom_pere: {
      required:"Le nom du père est requis.",
      min: "Le nom du père doit avoir au moins {length} caractères",
      max: "Le nom du père doit avoir au plus {length} caractères"
    },
    prenom_pere: {
      required:"Le prénom du père est requis.",
      min: "Le prénom du père doit avoir au moins {length} lettres",
      max: "Le prénom du père doit avoir au plus {length} lettres"
    },
    nom_mere: {
      required:"Le nom de la mère est requis.",
      min: "Le nom de la mère doit avoir au moins {length} lettres",
      max: "Le nom de la mère  doit avoir au plus {length} lettres"
    },
    prenom_mere: {
      required:"Le prénom de la mère est requis.",
      min: "Le prénom de la mère doit avoir au moins {length} lettres",
      max: "Le prénom de la mère  doit avoir au plus {length} lettres"
    },
    prenom: {
      required:"Le prénom  est requis.",
      min: "Le prénom doit avoir au moins {length} lettres",
      max: "Le prénom doit avoir au plus {length} lettres"
    },
    sexe: {
      required:"Veuillez préciser votre genre.",
    },
    date_naissance: {
      required:"La date de naissance est requise.",
      date_format:"La date de naissance a un format invalide.",
      before:"La date de naissance doit être antérieure au {date}",
      date_min:"La date de naissance minimale est le {date}"
    },
    situation_matrimoniale: {
      required:"La situation matrimoniale est requise.",
    },
    lieu_naissance: {
      required:"Le lieu de naissance est requis.",
      min: "Le lieu de naissance doit avoir au moins {length} caractère",
      max: "Le lieu de naissance  doit avoir au plus {length} caractères"
    },
    nip : {
      required:"Le NIP est requis.",
    },
    nationalite: {
      required:"La nationalité est requise.",
    },
    boite_postale: {
      required:"La boite postale est requise.",
      regex:"La boite postale doit contenir au plus 5 chiffres.",
      dontStartsWith:"La boite postale ne doit pas commencer par {start}."
    },
    telephone: {
      required:"Le n° de téléphone principal est requis.",
      regex:"Le n° de téléphone doit avoir un format gabonais."
    },
    telephone_alt: {
      required:"Le n° de téléphone alternatif est requis.",
      regex:"Le n° de téléphone doit avoir un format gabonais."

    },
    tuteur_nom: {
      required:"Le nom de la personne contact est requis.",
    },
    tuteur_prenom: {
      required:"Le prénom de la personne contact est requis.",
    },
    organisation_denomination: {
      required:"Le nom de la structure est requis.",
      max:"Le nom de la structure doit avoir au plus {length} caractères.",
    },
    organisation_adresse: {
      required:"L'adresse de la structure est requise.",
      max:"L'adresse de la structure doit avoir au plus {length} caractères.",
      
    },
    structure_id: {
      required:"La structure de tutelle est requise.",
    },
    personne_contact: {
      required:"La personne contact est requise.",
      max:"La personne contact  doit avoir au plus {length} caractères.",
    },
    captcha: {
      required:"Le recaptcha est requis.",
    },
  }

export default validations;