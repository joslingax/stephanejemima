import moment from "moment";



const isIncluded =  function(value,param)
{

    return param.includes(value.toLowerCase().trim())
  
}

const min =  function(value,param)
{
    value = (""+value).trim()
    var count = value.length
    return count >= param
  
};

const max =  function(value,param)
{
    value = (""+value).trim()
    var count = value.length
    return count <= param
  
};

const date =  function(value)
{
    return moment(value,'DD/MM/YYYY').isValid()
  
};

const required =  function(value)
{
    var result = true
    if(value == undefined) result = false
    else if(value == "") result = false
    else if(value == null) result = false
    return result
  
};


export default {  date, required , max, min, isIncluded} ;