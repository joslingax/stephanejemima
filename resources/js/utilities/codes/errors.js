

/**
 * Contient la liste des code HTTP d'erreurs avec le message correspondant
 * 
 * @author Joslin Issiga <joslin.issiga@aninf.ga>
 */
let codes = {
    500 : "Vous avez une erreur système.",
    422 : "Vous avez des erreurs dans votre formulaire.",
    404 : "Vous avez atteint une page ou une URL inconnue.",
    403 : "Vous n'êtes pas autorisé à effectuer cette action.",
    419 : "La validité du formulaire a expiré. Veuillez raffraichir la page.",
}

export default codes;