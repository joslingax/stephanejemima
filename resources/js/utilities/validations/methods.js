

/**
 * Contient des règles de validation customisées
 */
const included = {
  getMessage(field, args) {
    // will be added to default locale messages.
    return "ok"
  },
  validate(value, args) {
    return true;
  }
};

// Verifie qu'une chaine ne commence pas par un caractère ou groupe de caractères
// précis
const dontStartsWith = {
  validate(value, { start}) {
    value = ""+value
    return !value.startsWith(start);
  },
  params: ['start'],
  message: (fieldName, placeholders) => {
    return `Le champs ${fieldName} ne doit pas commencer par ${placeholders.start}`;
  }
}

// Verifie qu'une chaine commence par un caractère ou groupe de caractères
// précis
const startsWith = {
  validate(value, { start}) {
    value = ""+value
    return value.startsWith(start);
  },
  params: ['start'],
  message: (fieldName, placeholders) => {
    return `Le champs ${fieldName} ne doit pas commencer par ${placeholders.start}`;
  }
}
export default { included, dontStartsWith, startsWith  } ;