


let validations = {
    email: {
      required : "L'adresse e-mail est requise.",
      email:"Le format  de l'adresse e-mail est invalide"
    },
    password: {
      required : "Le mot de passe est requis.",
      min: " Le mot de passe doit avoir au moins {length} caractères"
      
    },
    password_confirmation: {
      required:"La confirmation du mot de passe est requise.",
      confirmed : "La confirmation du mot de passe ne corresponds pas.",
    },
    nom: {
      required:"Le nom de l'apprenant est requis.",
      min: "Le nom de l'apprenant doit avoir au moins {length} caractères",
      max: "Le nom de l'apprenant doit avoir au plus {length} caractères"
    },
    nom_rue: {
      required:"Le nom de la rue est requis.",
      min: "Le nom de la rue doit avoir au moins {length} caractères",
      max: "Le nom de la rue doit avoir au plus {length} caractères"
    },
    nr_rue: {
      required:"Le numéro de la rue est requis.",
      regex:'Le numéro doit contenir au plus 3 chiffres.',
      dontStartsWith:'Le n° de rue ne doit pas commencer par {start}.'
    },
    est_boursier: {
      oneOf:"Seules les valeurs 'vrai' et 'faux' sont acceptées",
      required:"Précisez le statut boursier de l'apprenant."
    },
    est_salarie: {
      oneOf:"Seules les valeurs 'vrai' et 'faux' sont acceptées",
      required:"Précisez le statut salarié de l'apprenant."
    },
    boite_postale: {
      required:"La boite postale de la rue est requise.",
    },
    ville_id: {
      required:"La ville de résidence est requise.",
      exists:"Cette ville est inconnue.",
    },
    quartier_id: {
      required:"Le quartier de résidence est requis.",
      exists:"Ce quartier est inconnue.",
    },
    nom_pere: {
      required:"Le nom du père est requis.",
      min: "Le nom du père doit avoir au moins {length} caractères",
      max: "Le nom du père doit avoir au plus {length} caractères"
    },
    prenom_pere: {
      required:"Le prénom du père est requis.",
      min: "Le prénom du père doit avoir au moins {length} caractères",
      max: "Le prénom du père doit avoir au plus {length} caractères"
    },
    nom_mere: {
      required:"Le nom de la mère est requis.",
      min: "Le nom de la mère doit avoir au moins {length} caractères",
      max: "Le nom de la mère  doit avoir au plus {length} caractères"
    },
    prenom_mere: {
      required:"Le prénom de la mère est requis.",
      min: "Le prénom de la mère doit avoir au moins {length} caractères",
      max: "Le prénom de la mère  doit avoir au plus {length} caractères"
    },
    prenom: {
      required:"Le prénom de l'apprenant est requis.",
      min: "Le prénom de l'apprenant doit avoir au moins {length} caractères",
      max: "Le prénom de l'apprenant doit avoir au plus {length} caractères"
    },
    sexe: {
      required:"Veuillez préciser votre genre.",
    },
    date_naissance: {
      required:"La date de naissance est requise.",
      date_format:"La date de naissance a un format invalide."
    },
    situation_matrimoniale: {
      required:"La situation matrimoniale est requise.",
    },
    lieu_naissance: {
      required:"Le lieu de naissance est requis.",
    },
    nip : {
      required:"Le NIP est requis.",
    },
    nationalite: {
      required:"La nationalité est requise.",
    },
    boite_postale: {
      regex:"La boite postale doit contenir au plus 5 chiffres.",
      dontStartsWith:"La boite postale ne doit pas commencer par {start}."
    },
    telephone: {
      required:"Le numéro de téléphone principal est requis.",
      regex:"Le numéro de téléphone doit avoir un format gabonais."
    },
    telephone_alt: {
      required:"Le numéro de téléphone alternatif est requis.",
      regex:"Le numéro de téléphone doit avoir un format gabonais."

    },
    tuteur_nom: {
      required:"Le nom de la personne contact est requis.",
    },
    tuteur_prenom: {
      required:"Le prénom de la personne contact est requis.",
    }
  }

export default validations;